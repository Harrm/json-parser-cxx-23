#include <print>
#include <ranges>
#include <stacktrace>

#include "json_parser.hpp"

static_assert(std::input_or_output_iterator<dts::generator<std::expected<json_parser::Token, json_parser::LexerError>>::iterator>);
static_assert(std::ranges::viewable_range<dts::generator<std::expected<json_parser::Token, json_parser::LexerError>>>);

int main() {
    auto tokens = json_parser::run_lexer("   \r{ \"a\": 3 , \"b\": null , \"c\":false, \"d\": \"aad a\", \"e\":{}, \"f\":[-1,2,false ], \"g\": -32.43 }   ");
    auto res = json_parser::run_parser(
             std::views::transform(tokens, [](std::expected<json_parser::Token, json_parser::LexerError> const &e) {
                if (e.has_value()) return e.value();
                std::print("at {}: {}\n{}", e.error().location, e.error().message, std::stacktrace::current());
                std::exit(-1);
            }));
    if (!res) {
        std::print("error at {}: {}", res.error().location, res.error().message);
    }
}
