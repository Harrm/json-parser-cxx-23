#pragma once

#include <ranges>
#include <coroutine>

namespace dts
{

    template <typename Ref, typename Val = void, typename Alloc = void>
    class generator : public std::ranges::view_interface<generator<Ref, Val, Alloc>>
    {
        using value = std::conditional_t<std::is_void_v<Val>, std::remove_cvref_t<Ref>, Val>;
        using reference = std::conditional_t<std::is_void_v<Val>, Ref &&, Ref>;

    public:
        using yielded = std::conditional_t<std::is_reference_v<reference>, reference, const reference &>;

        class promise_type
        {
            friend class generator::iterator;
        public:
            generator get_return_object()
            {
                return generator{std::coroutine_handle<promise_type>::from_promise(*this)};
            }
            std::suspend_always initial_suspend() const noexcept { return {}; }
            std::suspend_always final_suspend() noexcept { return {}; }
            std::suspend_always yield_value(yielded val) noexcept
            {
                value_ = std::addressof(val);
                return {};
            }

            void return_void() const noexcept {}

            void unhandled_exception() { throw; }

        private:
            std::add_pointer_t<generator::yielded> value_{};
        };

        generator(std::coroutine_handle<promise_type> coroutine) : coroutine_{coroutine} {}

        class iterator
        {
        public:
            using value_type = generator::value;
            using difference_type = std::ptrdiff_t;

            iterator(std::coroutine_handle<promise_type> coroutine) : coroutine_{ coroutine } {}

            iterator(iterator&& other) noexcept {
                coroutine_ = std::exchange(other.coroutine_, {});
            }

            iterator &operator=(iterator &&other) noexcept
            {
                coroutine_ = std::exchange(other.coroutine_, {});
                return *this;
            }

            generator::reference operator*() const noexcept(std::is_nothrow_copy_constructible_v<generator::reference>)
            {
                auto &promise = coroutine_.promise();
                return static_cast<reference>(*promise.value_);
            }

            constexpr iterator &operator++() {
                coroutine_.resume();
                return *this;
            }

            constexpr void operator++(int) {
                return ++*this;
            }

            friend bool operator==( const iterator& i, std::default_sentinel_t ) {
                return i.coroutine_.done();
            }

        private:
            std::coroutine_handle<promise_type> coroutine_;
        };

        iterator begin()
        {
            coroutine_.resume();
            return iterator{coroutine_};
        }

        auto end() const noexcept
        {
            return std::default_sentinel;
        }

    private:
        std::coroutine_handle<promise_type> coroutine_;
    };

}