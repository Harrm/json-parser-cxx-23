#pragma once

#include <format>
#include <string_view>
#include <expected>
#include <string>
#include "generator.hpp"
#include <ranges>
#include <map>
#include <vector>
#include <variant>

namespace json_parser {

    struct LexerError {
        uint32_t location;
        std::string message;
    };

    struct Token {
        enum Type {
            String,
            BeginArray,
            EndArray,
            Comma,
            Colon,
            BeginObject,
            EndObject,
            True, False, Null,
            Number,
        } type;
        uint32_t location;
        std::string_view str;
    };


    dts::generator<std::expected<Token, LexerError>> run_lexer(std::string_view str);

    struct Value;
    struct String;

    struct Object {
        std::map<String, Value> members;
    };

    struct Array {
        std::vector<Value> elements;
    };

    struct String {
        auto operator<=>(const String&) const = default;

        std::string_view str;
    };

    struct Number {
        double value;
    };

    enum class Literal {
        True, False, Null
    };

    struct Value {
        std::variant<Object, Array, String, Number, Literal> data;

        Value(): data{Literal::Null} {}

        template<typename T> requires (std::constructible_from<decltype(data), T&&>)
        Value(T &&t): data{std::forward<T>(t)} {}

        Value(Value&&v): data{std::move(v.data)} {}
        Value& operator=(Value&&v) { data = std::move(v.data); return *this; }
        Value(const Value&v): data{v.data} {}
        Value& operator=(const Value&v) { data = v.data; return *this; }

    };

    template<typename T>
    struct Parser {
    };

    struct ParserError {
        uint32_t location;
        std::string message;
    };

    template<>
    struct Parser<Array> {
        template<typename R>
        static std::expected<std::pair<Array, std::ranges::iterator_t<R>>, ParserError>
        parse(R &&range);
    };

    template<>
    struct Parser<Object> {
        template<typename R>
        static std::expected<std::pair<Object, std::ranges::iterator_t<R>>, ParserError>
            parse(R&& range);
    };
    
    template<>
    struct Parser<Number> {
        template<typename R>
        static std::expected<std::pair<Number, std::ranges::iterator_t<R>>, ParserError>
            parse(R&& range);
    };

    template<>
    struct Parser<Value> {
        template<typename R>
        std::expected<std::pair<Value, std::ranges::iterator_t<R>>, ParserError>
        static parse(R &&range) {
            auto iter = std::begin(range);
            auto end = std::end(range);
            if (iter == end) return std::unexpected(ParserError{0, "Unexpected end of value"});
            auto t = *iter;
            switch (t.type) {
                case json_parser::Token::String: {
                    return std::pair<Value, std::ranges::iterator_t<R>>{String{t.str}, std::move(++iter)};
                }
                case json_parser::Token::BeginArray: {
                    auto res = Parser<Array>::parse(std::ranges::subrange(std::move(iter), end));
                    return res.transform([](auto& p) {
                        return std::pair{ Value{p.first}, std::move(p.second) };
                    });
                }
                case json_parser::Token::True:
                    return std::pair{ Literal::True, std::move(++iter) };
                case json_parser::Token::False:
                    return std::pair{ Literal::False, std::move(++iter) };
                case json_parser::Token::Null:
                    return std::pair{ Literal::Null, std::move(++iter) };
                case json_parser::Token::Number:
                {
                    auto res = Parser<Number>::parse(std::ranges::subrange(std::move(iter), end));
                    return res.transform([](auto& p) {
                        return std::pair{ Value{p.first}, std::move(p.second) };
                    });
                }
                case json_parser::Token::BeginObject:
                {
                    auto res = Parser<Object>::parse(std::ranges::subrange(std::move(iter), end));
                    return res.transform([](auto& p) {
                        return std::pair{ Value{p.first}, std::move(p.second) };
                    });
                }

                case json_parser::Token::EndArray:
                case json_parser::Token::Comma:
                case json_parser::Token::Colon:
                case json_parser::Token::EndObject:
                    return std::unexpected(ParserError{t.location, std::format("Unexpected token {}", t)});
            }
        }
    };

    template<typename R>
    std::expected<std::pair<Array, std::ranges::iterator_t<R>>, ParserError>
    Parser<Array>::parse(R &&range) {
        auto iter = std::begin(range);
        auto end = std::end(range);
        if (iter == end) return std::unexpected(ParserError{0, "Unexpected end of array"});
        auto t = *iter;
        if (t.type != Token::BeginArray) {
            return std::unexpected(ParserError{t.location, "Arrays must start with ["});
        }
        iter++;
        if (iter == end) return std::unexpected(ParserError{0, "Unexpected end of array"});
        Array arr;
        while ((*iter).type != Token::EndArray) {
            auto res = Parser<Value>::parse(std::ranges::subrange(std::move(iter), end));
            if (!res) return std::unexpected(res.error());
            auto& [value, it] = res.value();
            iter = std::move(it);
            arr.elements.emplace_back(std::move(value));
            if ((*iter).type == Token::EndArray) break;
            if ((*iter).type != Token::Comma) {
                return std::unexpected(ParserError{(*iter).location, "Expected comma"});
            }
            iter++;
            if (iter == end) return std::unexpected(ParserError{0, "Unexpected end of array"});
        }
        return std::pair{std::move(arr), std::move(++iter)};
    }

    template<typename R>
    std::expected<std::pair<Object, std::ranges::iterator_t<R>>, ParserError>
    Parser<Object>::parse(R &&range) {
        auto iter = std::begin(range);
        auto end = std::end(range);
        if (iter == end) return std::unexpected(ParserError{0, "Unexpected end of object"});
        auto t = *iter;
        if (t.type != Token::BeginObject) {
            return std::unexpected(ParserError{t.location, "Objects must start with {"});
        }
        iter++;
        if (iter == end) return std::unexpected(ParserError{0, "Unexpected end of object"});
        Object obj;
        while ((*iter).type != Token::EndObject) {
            if ((*iter).type != Token::String) return std::unexpected(ParserError{(*iter).location, "Expected a string object key"});
            String key{(*iter).str};
            iter++;
            if (iter == end || (*iter).type == Token::EndObject) return std::unexpected(ParserError{0, "Unexpected end of object"});
            if ((*iter).type != Token::Colon) return std::unexpected(ParserError{(*iter).location, "Expected colon"});
            iter++;
            auto res = Parser<Value>::parse(std::ranges::subrange(std::move(iter), end));
            if (!res) return std::unexpected(res.error());

            obj.members[key] = std::move(res.value().first);
            iter = std::move(res.value().second);

            if ((*iter).type == Token::EndObject) break;
            if ((*iter).type != Token::Comma) {
                return std::unexpected(ParserError{(*iter).location, "Expected comma"});
            }
            iter++;
            if (iter == end) return std::unexpected(ParserError{0, "Unexpected end of array"});
        }
        return std::pair{std::move(obj), std::move(++iter)};
    }

    template<typename R>
    std::expected<std::pair<Number, std::ranges::iterator_t<R>>, ParserError>
    Parser<Number>::parse(R &&range) {
        int sign = 1;
        int exp_sign = 1;
        auto it = range.begin();
        auto token = *it;
        auto str = token.str;
        if (token.type != Token::Number) return std::unexpected(ParserError{token.location, "Expected number"});
        if (str.empty()) return std::unexpected(ParserError{token.location, "Expected '-' or digit"});
        auto str_it = str.begin();
        if (*str_it == '-') { sign = -1; str_it++; }

        if (str_it == str.end()) {
            return std::unexpected(ParserError{token.location, "Expected a digit"});
        }
        double value = 0.0;
        if (*str_it == '0') {
            str_it++;
        } else if (isdigit(*str_it)) {
            while(str_it != str.end() && isdigit(*str_it)) {
                value += *str_it - '0';
                value *= 10.0;
                str_it++;
            }
            value /= 10.0;
        } else {
            return std::unexpected(ParserError{token.location, "Expected a digit"});
        }
        if (str_it != str.end() && *str_it == '.') {
            str_it++;
            double fraction = 10.0;
            while(str_it != str.end() && isdigit(*str_it)) {
                value += (double)(*str_it - '0') / fraction;
                fraction *= 10.0;
                str_it++;
            }
        }
        if (str_it != str.end()) {
            return std::unexpected(ParserError{token.location, "Number is not fully parsed"});
        }
        return std::pair{Number{sign * value}, std::move(++it)};
    }

    template<typename Tokens>
    std::expected<Value, ParserError> run_parser(Tokens &&tokens) {
        return Parser<Value>::parse(tokens).transform([](auto pair) { return std::move(pair.first); });
    }
}

template<>
struct std::formatter<json_parser::Token, char> {
    template<class ParseContext>
    constexpr ParseContext::iterator parse(ParseContext &ctx) {
        auto it = ctx.begin();
        if (it == ctx.end()) return it;
        if (*it != '}') throw std::format_error{"json_parser::Token has no formatting parameters"};
        return it;
    }

    template<class FmtContext>
    FmtContext::iterator format(json_parser::Token const &t, FmtContext &ctx) const {
        std::string_view type;
        switch (t.type) {
           case json_parser::Token::String:
                type = "string";
                break;
            case json_parser::Token::BeginArray:
                type = "[";
                break;
            case json_parser::Token::EndArray:
                type = "]";
                break;
            case json_parser::Token::Comma:
                type = ",";
                break;
            case json_parser::Token::Colon:
                type = ":";
                break;
            case json_parser::Token::BeginObject:
                type = "{";
                break;
            case json_parser::Token::EndObject:
                type = "}";
                break;
            case json_parser::Token::True:
                type = "true";
                break;
            case json_parser::Token::False:
                type = "false";
                break;
            case json_parser::Token::Null:
                type = "null";
                break;
            case json_parser::Token::Number:
                type = "number";
                break;
        }
        return std::format_to(ctx.out(), "<{}>'{}'", type, t.str);
    }
};

template<>
struct std::formatter<json_parser::LexerError, char> {
    template<class ParseContext>
    constexpr ParseContext::iterator parse(ParseContext &ctx) {
        auto it = ctx.begin();
        if (*it != '}') throw std::format_error{"json_parser::LexerError has no formatting parameters"};
        return it;
    }

    template<class FmtContext>
    FmtContext::iterator format(json_parser::LexerError const &e, FmtContext &ctx) const {
        return std::format_to(ctx.out(), "{}", e.message);
    }
};