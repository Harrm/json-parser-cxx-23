
#include <optional>

#include "json_parser.hpp"

namespace json_parser {

    std::optional<Token::Type> detect_character_type(char c) {
        if (c == '"') return Token::String;
        if (c == '[') return Token::BeginArray;
        if (c == ']') return Token::EndArray;
        if (c == ',') return Token::Comma;
        if (c == ':') return Token::Colon;
        if (c == '{') return Token::BeginObject;
        if (c == '}') return Token::EndObject;
        if (c == 't') return Token::True;
        if (c == 'f') return Token::False;
        if (c == 'n') return Token::Null;
        if (c == '-') return Token::Number;
        if (std::isdigit(c)) return Token::Number;
        return std::nullopt;
    }

    std::expected<uint32_t, uint32_t> check_literal(std::string_view str, Token::Type type) {
        std::string_view literal;
        switch (type) {
            case Token::True:
                literal = "true";
                break;
            case Token::False:
                literal = "false";
                break;
            case Token::Null:
                literal = "null";
                break;
            default:
                return std::unexpected(1);
        }
        if (str.size() < literal.size()) {
            return std::unexpected(str.size());
        }
        if (str.substr(0, literal.size()) != literal) {
            return std::unexpected(literal.size());
        }
        return literal.size();
    }

    uint32_t skim_digits(std::string_view str) {
        if (str[0] == '0') {
            return 1;
        } else {
            uint32_t n = 0;
            while (isdigit(str[n])) {
                n++;
            }
            return n;
        }
    }

    bool is_whitespace(char c) {
        return c == ' ' || c == '\r' || c == '\t' || c == '\n';
    }

    dts::generator<std::expected<Token, LexerError>> run_lexer(std::string_view str) {
        uint32_t position{};
        while (position < str.size()) {
            while (position < str.size() && is_whitespace(str[position])) position++;
            if (position == str.size()) break;
            auto type = detect_character_type(str[position]);

            if (!type) {
                co_yield std::unexpected{LexerError{position, std::format("Unexpected character '{}'", str[position])}};
                co_return;
            }
            switch (*type) {
                case Token::String: {
                    uint32_t n = 1;
                    while (position + n < str.size() && str[position + n] != '"') {
                        if (str[position + n] == '\\') {
                            n++;
                            if (position + n == str.size()) {
                                co_yield std::unexpected(LexerError{position + n, "Unexpected end of string"});
                                co_return;
                            }
                        }
                        n++;
                    }
                    if (str[position + n] != '"') {
                        co_yield std::unexpected(LexerError{position + n, "Unexpected end of string"});
                        co_return;
                    }
                    co_yield Token{Token::String, position, str.substr(position, n + 1)};
                    position += n + 1;
                    break;
                }
                case Token::BeginArray:
                case Token::EndArray:
                case Token::Comma:
                case Token::Colon:
                case Token::BeginObject:
                case Token::EndObject:
                    co_yield Token{*type, position, str.substr(position, 1)};
                    position++;
                    break;
                case Token::True:
                case Token::False:
                case Token::Null:
                    if (auto res = check_literal(str.substr(position), *type); res) {
                        co_yield Token{*type, position, str.substr(position, res.value())};
                        position += res.value();
                        break;
                    } else {
                        co_yield std::unexpected(LexerError{position, std::format(
                                "Unexpected token '{}', perhaps you meant 'true'?",
                                str.substr(position, res.error()))});
                        co_return;
                    }

                case Token::Number: {
                    uint32_t n = 0;
                    if (str[position] == '-') {
                        n++;
                        if (position + n == str.size()) {
                            co_yield std::unexpected(LexerError{position + n, "Unexpected end of number"});
                            co_return;
                        }
                    }
                    n += skim_digits(str.substr(position + n));
                    if (str[position + n] == '.') {
                        n++;
                        n += skim_digits(str.substr(position + n));
                    }
                    if (str[position + n] == 'e' || str[position + n] == 'E') {
                        n++;
                        if (str[position + n] == '+') {
                            n++;
                        }
                        if (str[position + n] == '-') {
                            n++;
                        }
                        n += skim_digits(str.substr(position + n));
                    }
                    co_yield Token{*type, position, str.substr(position, n)};
                    position += n;
                    break;
                }
            }
        }
    }

}
